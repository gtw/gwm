#ifndef FRAME_H
#define FRAME_H

extern struct gwm_window *focus_frame; /* the WINDOW_FRAME which has the
					  focus, if any */

extern int frame_t( struct gwm_window *window, int include_x_border );
extern int frame_b( struct gwm_window *window, int include_x_border );
extern int frame_l( struct gwm_window *window, int include_x_border );
extern int frame_r( struct gwm_window *window, int include_x_border );
extern int frame_xb( struct gwm_window *window );

/* Change the _NET_FRAME_EXTENTS property on the client. */
extern void update_frame_extents( struct gwm_window *window );

extern void translate_child_to_frame( struct gwm_window *frame,
				      int *fx, int *fy, int *fwidth,
				      int *fheight, int cx, int cy,
				      int cwidth, int cheight,
				      int cborder, int win_gravity );

extern void translate_frame_to_child( struct gwm_window *frame,
				      int *cx, int *cy, int fx, int fy,
				      int cborder, int win_gravity );

extern void apply_size_constraints( struct gwm_window *window, int *width,
				    int *height );

extern void activate_focus_frame( xcb_timestamp_t time );
extern void deactivate_focus_frame( void );

extern void synthetic_configure_notify( struct gwm_window *window );
    
extern const event_handler frame_handlers[], childless_handlers[];

#endif
