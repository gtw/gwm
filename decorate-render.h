#ifndef DECORATE_RENDER_H
#define DECORATE_RENDER_H

extern void render_update_window( struct gwm_window *window );
extern void render_window_size( struct gwm_window *window, int *width,
				int *height );
extern void render_replace_icons( struct gwm_window *window, int num_icons,
				  int *widths, int *heights, uint32_t **icons,
				  xcb_pixmap_t *bitmaps );

extern int decorate_render_init( void );
extern void decorate_render_done( void );

#endif
