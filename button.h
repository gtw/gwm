#ifndef BUTTON_H
#define BUTTON_H

extern int button_size( struct gwm_window *window, int include_x_border );
extern int button_xb( struct gwm_window *window );

extern const event_handler button_handlers[];

#endif
