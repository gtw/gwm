#ifndef DECORATE_CORE_H
#define DECORATE_CORE_H

extern void core_update_window( struct gwm_window *window );
extern void core_window_size( struct gwm_window *window, int *width,
			      int *height );
extern void core_replace_icons( struct gwm_window *window, int num_icons,
				int *widths, int *heights, uint32_t **icons,
				xcb_pixmap_t *bitmaps );

extern void decorate_core_init( void );
extern void decorate_core_done( void );

#endif
