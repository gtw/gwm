/*
 * image.c
 *
 * Part of gwm, the Gratuitous Window Manager,
 *     by Gary Wong, <gtw@gnu.org>.
 *
 * Copyright (C) 2009  Gary Wong
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of version 3 of the GNU General Public License as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */

#include <config.h>

#include <xcb/xcb.h>

#include "gwm.h"

#include "image.h"

extern xcb_void_cookie_t put_image( xcb_drawable_t drawable, xcb_gcontext_t gc,
				    uint16_t width, uint16_t height, int16_t x,
				    int16_t y, uint8_t left_pad, uint8_t depth,
				    uint32_t len, const uint8_t *data ) {

    static uint16_t max;

    if( !max )
	max = xcb_get_setup( c )->maximum_request_length << 2;

    if( len + sizeof (xcb_put_image_request_t ) < max )
	return xcb_put_image( c, XCB_IMAGE_FORMAT_Z_PIXMAP, drawable,
			      gc, width, height, x, y, left_pad,
			      depth, len, data );
    else {
	int linesize = len / height;
	int lines = ( max - sizeof (xcb_put_image_request_t) ) / linesize;

	while( height > lines ) {
	    xcb_put_image( c, XCB_IMAGE_FORMAT_Z_PIXMAP, drawable,
			   gc, width, lines, x, y, left_pad,
			   depth, lines * linesize, data );
	    y += lines;
	    height -= lines;
	    len -= lines * linesize;
	    data += lines * linesize;
	}

	return xcb_put_image( c, XCB_IMAGE_FORMAT_Z_PIXMAP, drawable,
			      gc, width, height, x, y, left_pad,
			      depth, len, data );
    }
}
